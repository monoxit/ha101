/*コメント
  This scketch is based on mqtt_esp8266.ino included with the pubsubclient by Nick O'Leary
  pubsubclientライブラリに添付のmqtt_esp8266.ino by Nick O'Learyに基づく
  Copyright (c) 2019 Masami Yamakawa
   setup_wifi(), reconnect() and parts of callback() functions are
     Copyright (c) 2008-2015 Nicholas O'Leary
  This software is released under the MIT License.
  http://opensource.org/licenses/mit-license.php
*/

/*
   ESP8266WiFiとPubSubClient（MQTTクライアント）ライブラリ（あらかじめ作られたプログラム集）
   を使うと指定
*/
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

// 次をネットワーク環境に合わせて変更する
const char* ssid = "MONOXIT";
const char* password = "secret12";
const char* mqttServer = "192.168.3.111";
const char* mqttOndoTopic = "home/sensor99/ondo/state";
const char* mqttMotionTopic = "home/sensor99/motion/state";
const char* mqttLedCommandTopic = "home/sensor99/led/command";
const char* mqttLedStateTopic = "home/sensor99/led/state";
const char* mqttDevId = "HS999";

const int   irMotionPin = 14;
const int   dht11Pin = 5;
const int   ledPin = 13;

// 状態更新間隔（ミリ秒）を設定
const int stateUpdateMillis = 10 * 1000;

// ライブラリの準備
WiFiClient espClient;
PubSubClient client(espClient); // MQTTクライアント機能をclientという名前で使う
// DHT11センサライブラリをdht11という名前で使うようにする
DHT dht11(dht11Pin, DHT11);

// 最後に状態をパブリッシュしたときのマイコン起動時からの経過ミリ秒数を0に初期化
unsigned long lastPublishMillis = 0;

int lastReportedIrMotion = LOW;
int lastReportedLedState = LOW;

// 電源ON後1度だけ実行されるプログラムの部分
// {から}が１度だけ実行される
void setup() {
  pinMode(ledPin, OUTPUT);     // LEDが接続されている端子（ピン）を出力モードに設定
  Serial.begin(115200);
  setupWifi(); //WiFiに接続

  // MQTTクライアントに接続先サーバーとポート番号を設定
  client.setServer(mqttServer, 1883);
  // MQTTクライアントに、メッセージを受信したときに実行したい
  // プログラムの部分を教える
  client.setCallback(callback);

  // DHT11との通信を開始できる状態にする
  dht11.begin();
}

// WiFiの接続をする部分
// プログラムの他の部分でsetupWiFi()を記述すると実際にはここが動く
void setupWifi() {

  delay(10);
  // WiFiへの接続開始
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// メッセージ受信したときに実行させる部分
// payload（積み荷）に受信したメッセージが入っている
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived ["); //Serial...の部分はプログラムの主要部分でない
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // １を受け取ったらLEDをONする
  if ((char)payload[0] == '1') {
    digitalWrite(ledPin, HIGH);
  } else { //そうでなければ LEDをOFFにする
    digitalWrite(ledPin, LOW);
  }
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイス名HS111でMQTTサーバーへ接続開始
    if (client.connect(mqttDevId)) {
      // 接続できたら
      Serial.println("connected");
      // mqttLedCommandTopicに申し込み
      client.subscribe(mqttLedCommandTopic);
    } else {
      // 接続できなかったら
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // ５秒待ってリトライ
      delay(5000);
    }
  }
}

void publishState(const char* topic, String label, int value) {
  // JSON形式のデータ{"led":1}を作り、LEDの状態をパブリッシュ
  String payload = "{\"";
  payload += label;
  payload += "\":";
  payload += value;
  payload += "}";
  //人感センサの状態をtopicにパブリッシュ
  Serial.print(topic);
  Serial.print(":");
  Serial.println(payload);

  // 指定されたトピックで値をパブリッシュ
  // パラメタ最後のtrueはMQTTのretain（保持）オプションをONにする指定
  // 保持オプションをONにするとMQTTブローカはこのメッセージを保持し、
  // サブスクライバがサブスクライブ後にパブリッシュしてくれる
  client.publish(topic, (char*) payload.c_str(), true);
}

// 電源がOFFにされるかリセットされるまで永久に繰り返されるプログラムの部分
void loop() {

  // もしMQTTブローカに接続していなかったらreconnect部分を動かす
  if (!client.connected()) {
    reconnect();
  }
  // MQTTの裏方の処理を動かすためclient.loopを動かす
  client.loop();

  // LEDの現在の状態をパブリッシュ
  int ledState = digitalRead(ledPin);
  // 最後にパブリッシュしたLEDの状態と現在の状態が異なっていたら
  if (lastReportedLedState != ledState) {
    lastReportedLedState = ledState;
    // publishStateを動かしてLEDの状態をパブリッシュする
    publishState(mqttLedStateTopic, "led", ledState);
  }

  // 人感センサの状態をパブリッシュ
  int irMotion = digitalRead(irMotionPin);
  // 最後のパブリッシュした人感センサの状態と現在の状態が異なっていたら
  if (irMotion != lastReportedIrMotion) {
    lastReportedIrMotion = irMotion;
    // publishStateを動かして人感センサの状態をパブリッシュする
    publishState(mqttMotionTopic, "motion", irMotion);
  }

  // 定期的に状態をパブリッシュ
  unsigned long now = millis(); //millis()で電源ONからの経過ミリ秒数を取得
  if (now - lastPublishMillis > stateUpdateMillis) {
    lastPublishMillis = now;

    publishState(mqttLedStateTopic, "led", ledState);
    publishState(mqttMotionTopic, "motion", irMotion);

    // 温度湿度のパブリッシュ
    byte temperature = 0;
    byte humidity = 0;
    // センサー値を読み込む（小数点付きの数値）
    float h = dht11.readHumidity();
    float t = dht11.readTemperature();

    // もし読み取り不良なら
    if (isnan(h) || isnan(t)) {
      Serial.print("Failed to read from DHT sensor!");

      // 読み取り成功なら
    } else {

      // 小数点以下切り捨てて整数のondo, situdoに代入
      temperature = t;
      humidity = h;

      // JSON形式のデータ{"t":25, "h":53}を作る
      String payload = "{\"t\":";
      payload += (int8_t)temperature;
      payload += ",\"h\":";
      payload += (uint8_t)humidity;
      payload += "}";

      Serial.print("Publish message: ");
      Serial.println(payload);

      // トピックmqttOndoTopicにJSON形式の温度湿度をパブリッシュ
      client.publish(mqttOndoTopic, (char*) payload.c_str());
    }
  }
}
