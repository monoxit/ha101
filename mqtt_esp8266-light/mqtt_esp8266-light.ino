/*コメント
* PubSubClientライブラリに付属のBasic ESP8266 MQTT exampleを使用
* センサー値をJSON形式でパブリッシュ（投稿）するように修正したもの
*/

/*
 * ESP8266WiFiとPubSubClient（MQTTクライアント）ライブラリ（あらかじめ作られたプログラム集）
 * を使うと指定
 */
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// 次をネットワーク環境に合わせて変更する

const char* ssid = "MONOXIT";
const char* password = "???";
const char* mqttServer = "192.168.133.xx";

// ライブラリの準備
WiFiClient espClient;
PubSubClient client(espClient); //MQTTクライアント機能をclientという名前で使う

long lastMsg = 0;
char msg[50];
int value = 0;

// 電源ON後1度だけ実行されるプログラムの部分
// {から}が１度だけ実行される
void setup() {
  pinMode(13, OUTPUT);     // 13ピンを出力モードに設定
  Serial.begin(115200);
  setupWifi(); //WiFiに接続
  // MQTTクライアントに、接続先サーバーとポート番号を設定
  client.setServer(mqttServer, 1883); 
  // MQTTクライアントに、メッセージを受信したときに実行したい
  // プログラムの部分を設定
  client.setCallback(callback); 
}

// WiFiの接続をする部分
// プログラムの他の部分でsetupWiFi()を記述すると実際にはここが動く
void setupWifi() {

  delay(10);
  // WiFiへの接続開始
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// メッセージ受信したときに実行させる部分
// payload（積み荷）に受信したメッセージが入っている
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived ["); //Serial...の部分はプログラムの主要部分でない
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  // 主要部分はここだけ
  // １を受け取ったらLEDをONする
  if ((char)payload[0] == '1') {
    digitalWrite(13, HIGH);
  } else { //そうでなければ LEDをOFFにする
    digitalWrite(13, LOW);
  }
}

// MQTTサーバーへ接続する部分
void reconnect() {
  // 接続できるまで繰り返す
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // デバイス名HS001でMQTTサーバーへ接続開始
    if (client.connect("HS001")) {
      //接続できたら
      Serial.println("connected");
      //トピックoutTopicにhello worldを投稿
      client.publish("outTopic", "hello world");
      // ... そしてトピックinTopicに申し込み
      client.subscribe("inTopic");
    } else {
      //接続できなかったら
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // ５秒待ってリトライ
      delay(5000);
    }
  }
}
// 電源がOFFにされるかリセットされるまで永久に繰り返されるプログラムの部分
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  long now = millis(); //millis()は電源ONからの経過ミリ秒数
  if (now - lastMsg > 5000) {
    // 最後の投稿から5秒以上経過したら
    lastMsg = now;
    // JSON形式{"d":{"id":"HS001","light":センサ値}}を投稿
    String payload = "{\"d\":{\"id\":\"";
    payload += "HS001";
    payload += "\",\"light\":";
    payload += analogRead(A0);
    payload += "}}";
    Serial.print("Publish message: ");
    Serial.println(payload);
    // トピックhome/sensor01にJSON形式で値を投稿
    client.publish("home/sensor01", (char*) payload.c_str());
  }
}