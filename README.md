# 「ホームオートメーション」サンプルスケッチ #

MONOxITの「ホームオートメーション」で使用のサンプルスケッチです。

## 使い方 ##

Arduino IDEのスケッチブックの保存場所にサンプルスケッチをコピーして使います。  
このbitbucketから、サンプルスケッチが含まれるZIP圧縮ファイルをダウンロードして、圧縮ファイル内のフォルダを、Arduino IDEのスケッチブックの保存場所にコピーします。

### コピーの手順 ###

* Webブラウザの画面を最大にする
* bitbucketのメニューから「ダウンロード」をクリック
* 「リポジトリをダウンロードする」をクリックしダウンロード
* ダウンロードフォルダに保存されたZIP圧縮ファイル内のmonoxit-ha101-0af434efad1cのような名前のフォルダ全体を、スケッチブックの保存場所にコピー  
※スケッチブックの保存場所はArduino IDEのメニューの「ファイル」の「環境設定」で表示される画面で確認できます。  
※フォルダ名の0af434efad1cの部分は異なる場合があります。

### サンプルスケッチの開き方 ###

Arduino IDEのメニューの「ファイル」の「スケッチブック」の「monoxit-ha101-0af434efad1c」からサンプルスケッチを開くことができます。
